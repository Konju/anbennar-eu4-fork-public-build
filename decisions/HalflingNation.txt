country_decisions = {

	halfling_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_small_country_flag }
			NOT = { exists = A97 } #Small Country doesn't exist
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			culture_group = halfling
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			#adm_tech = 10
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			#num_of_cities = 5
			num_of_owned_provinces_with = {
				value = 5
				region = small_country_region
				OR = {
					culture_group = halfling
				}
			}
		}
		effect = {
			change_tag = A97
			remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 2 }
				}
				set_government_rank = 2
			}
			small_country_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = A97
				
			}
			add_prestige = 25
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			set_country_flag = formed_small_country_flag
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
}