# These ideas are loaded after all country ideas, but before default idea.
# Hopefully this is loaded before anb_country_ideas!

gnollish_ideas = {
	start = {
		land_morale = 0.1
		may_perform_slave_raid = yes
	}
	
	bonus = {
		loot_amount = 0.20
	}
	
	trigger = {
		culture_group = gnollish
	}
	free = yes
	
	a_race_of_scavengers_and_raiders = {
		land_attrition = -0.1
		loot_amount = 0.10
	}
	pack_loyalty = {
		horde_unity = 1
		legitimacy = 1
	}
	demonic_rampage = {
		war_exhaustion = -0.02
		movement_speed = 0.15
	}
	kings_of_the_slave_trade = {
		trade_efficiency = 0.1
		production_efficiency = 0.10
	}
	tight_leash = {
		global_unrest = -1
		reduced_liberty_desire = 10
	}
	flesh_eaters = {
		hostile_attrition = 1
		land_maintenance_modifier = -0.10
	}
	legacy_of_the_xhazobine = {
		discipline = 0.1
		land_morale = 0.1
		ae_impact = -0.1
	}
}	

halfling_ideas = {
	start = {
		global_trade_goods_size_modifier = 0.1
		trade_efficiency = 0.1
	}
	
	bonus = {
		global_prov_trade_power_modifier = 0.15
	}
	
	trigger = {
		culture_group = halfling
	}
	free = yes
	
	halfling_quick_breeders = {
		manpower_recovery_speed = 0.2
	}
	halfling_agricultural_tradition = {
		production_efficiency = 0.1
	}
	halfling_guerrilla_warfare = {
		hostile_attrition = 1
	}
	halfling_human_hired_help = {
		merc_maintenance_modifier = -0.25
	}
	halfling_advice_of_retired_adventurers = {
		same_culture_advisor_cost = -0.25
	}
	halfling_courage_bravery_and_valour = {
		land_morale = 0.1
	}
	halfling_cuisine = {
		prestige = 1
	}
}	

adventurer_ideas = {
	start = {
		land_maintenance_modifier = -0.10
		land_attrition = -0.1
	}
	
	bonus = {
		manpower_recovery_speed = 0.1
	}
	
	trigger = {
		government = adventurer
	}
	free = yes
	
	adventurer_call_to_adventure = {
		global_manpower_modifier = 0.1
	}
	adventurer_develop_new_settlements = {
		colonists = 1
		development_cost = -0.2
	}
	adventurer_against_the_greentide = {
		culture_conversion_cost = -0.2
		war_exhaustion = -0.02
	}
	adventurer_the_new_frontier = {
		may_establish_frontier = yes
		auto_explore_adjacent_to_colony = yes
	}
	adventurer_fighting_spirit = {
		land_morale = 0.1
	}
	adventurer_legacy_of_heroes = {
		leader_land_shock = 1
	}
	adventurer_people_from_all_walks_of_life = {
		no_religion_penalty = yes
	}
}

orcish_ideas = {
	start = {
		may_establish_frontier = yes
		auto_explore_adjacent_to_colony = yes
	}
	
	bonus = {
		manpower_recovery_speed = 0.1
	}
	
	trigger = {
		culture_group = orcish
	}
	free = yes
	
	orcish_beserker_frenzy = {
		land_morale = 0.2
	}
	orcish_control_the_clans = {
		horde_unity = 1
		legitimacy = 1
	}
	orcish_teach_the_young = {
		manpower_recovery_speed = 0.1
	}
	orcish_self_sustaining_warbands = {
		land_attrition = -0.15
		land_maintenance_modifier = -0.05
	}
	orcish_natural_camoflague = {
		hostile_attrition = 1
	}
	orcish_orcish_legions = {
		army_tradition_decay = -0.01
		discipline = 0.1
	}
	orcish_veteran_mercenaries = {			#Mercenaries from abroad return home to impart advice
		mercenary_discipline = 0.05
		same_culture_advisor_cost = -0.25
	}
}

goblin_ideas = {
	start = {
		land_morale = 0.1
		may_perform_slave_raid = yes
	}
	
	bonus = {
		siege_ability = 0.10
	}
	
	trigger = {
		culture_group = goblin
	}
	free = yes
	
	goblin_always_prepared = {
		army_tradition_decay = -0.01
	}
	goblin_goblinic_austerity = {
		state_maintenance_modifier = -0.15
	}
	goblin_shamans = {
		legitimacy = 0.5
		hostile_attrition = 1
	}
	goblin_social_mobility = {
		advisor_cost = -0.05
		tolerance_heathen = 1
	}
	goblin_we_have_reserves = {
		infantry_cost = -0.1
		manpower_recovery_speed = 0.1
	}
	goblin_smug_pseudo_intellectuals = {
		technology_cost = -0.05
	}
	goblin_cowardly_tactics = {
		fire_damage = 0.10
	}
}	

sun_elf = {
	start = {
		land_morale = 0.1
		cav_to_inf_ratio = 0.15
	}
	
	bonus = {
		free_leader_pool = 1
	}
	
	trigger = {
		culture = sun_elf
	}
	free = yes
	
	sun_elf_purity_of_blood = {
		prestige = 0.5
	}
	sun_elf_flying_chariots = {
		leader_land_manuever = 1
		fire_damage = 0.10
	}
	sun_elf_discriminatory_magical_schooling = {
		global_unrest = -1
	}
	sun_elf_centuries_of_experience = {
		global_tax_modifier = 0.1
	}
	sun_elf_lavish_courts = {
		advisor_pool = 1
	}
	sun_elf_long_educations = {
		technology_cost = -0.10
	}
	sun_elf_experienced_plotters = {
		spy_offence = 0.2
	}
}
