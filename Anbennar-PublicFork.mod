#Move me from your mod folder to here

name="Anbennar"		
path="mod/anbennar-eu4-fork-public-build"	#Change this to whatever your file's called
tags={"Alternative History""Total Conversion""Fantasy"}

supported_version="1.24.*.*"

replace_path="localisation/prov_names_I_english"
replace_path="localisation/prov_names_adj_I_english"
replace_path="common/countries"
replace_path="common/bookmarks"
replace_path="common/province_names"
replace_path="common/government_names"
replace_path="events"
replace_path="history/diplomacy"
replace_path="history/wars"
replace_path="history/provinces"
picture="anbennar.jpg"
