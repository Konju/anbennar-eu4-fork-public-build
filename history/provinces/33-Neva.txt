#33 - Neva | 

owner = A87
controller = A87
add_core = A87
add_core = A78
religion = regent_court
culture = roilsardi

hre = yes

base_tax = 9
base_production = 11
base_manpower = 9

trade_goods = wine
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish