#72 - Breisgau

owner = A02
controller = A02
add_core = A02
culture = high_lorentish
religion = regent_court
base_tax = 4
base_production = 4
trade_goods = wine
base_manpower = 2
capital = "" 
is_city = yes
hre = no

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

