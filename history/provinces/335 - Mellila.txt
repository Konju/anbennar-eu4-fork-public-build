#335 - Melilla

owner = A25
controller = A25
add_core = A25
culture = moon_elf
religion = regent_court

hre = yes

base_tax = 4
base_production = 5
base_manpower = 3

trade_goods = copper

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = human_minority_integrated_large
	duration = -1
}