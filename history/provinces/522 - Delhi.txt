#522 - Azka-szel-Aska

owner = F18
controller = F18
add_core = F18
culture = bahari
religion = bulwari_sun_cult

base_tax = 2
base_production = 2
base_manpower = 3

trade_goods = wine

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
