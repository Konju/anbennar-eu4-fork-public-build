#535 - Tenkar

owner = F15
controller = F15
add_core = F15
add_core = F19
culture = sun_elf
religion = bulwari_sun_cult

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = wine

capital = ""

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_nationalism = 10