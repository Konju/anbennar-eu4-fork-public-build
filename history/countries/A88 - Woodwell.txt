government = feudal_monarchy
government_rank = 1
primary_culture = west_damerian
religion = regent_court
technology_group = tech_cannorian
capital = 22 # Woodwell
national_focus = DIP

1427.1.12 = {
	monarch = {
		name = "Humbert IV"
		dynasty = "Woodwell"
		birth_date = 1390.2.8
		adm = 1
		dip = 0
		mil = 1
	}
}