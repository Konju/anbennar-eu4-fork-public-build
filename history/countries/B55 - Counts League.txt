government = feudal_monarchy
government_rank = 1
primary_culture = castellyrian
religion = regent_court
technology_group = tech_cannorian
capital = 874
national_focus = DIP

1400.1.2 = { set_country_flag = is_a_county }