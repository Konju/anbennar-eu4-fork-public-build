government = knightly_order
government_rank = 1
primary_culture = corintari
religion = regent_court_corin
technology_group = tech_cannorian
capital = 827
historical_rival = B01 #Greentide

1444.2.10 = {
	monarch = {
		name = "Lothane Bluetusk"
		culture = half_orc
		birth_date = 1425.7.3
		adm = 3
		dip = 2
		mil = 6
	}
	add_ruler_personality = inspiring_leader_personality
}
