government = oligarchic_republic
government_rank = 1
primary_culture = cliff_gnome
religion = the_thought
technology_group = tech_gnomish
capital = 169 #Nimscodd
national_focus = DIP

1401.6.11 = {
	monarch = {
		name = "Mordibam"
		dynasty = "Quintain"
		birth_date = 1204.5.3
		adm = 4
		dip = 5
		mil = 6
	}
	add_ruler_personality = immortal_personality
	add_ruler_personality = tactical_genius_personality
	set_ruler_flag = set_immortality
}