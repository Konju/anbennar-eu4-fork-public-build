government = monastic_order_government
government_rank = 1
primary_culture = east_damerian
religion = regent_court
technology_group = tech_cannorian
capital = 265


1393.1.1 = {
	monarch = {
		name = "Konrad von Jungingen"
		birth_date = 1355.1.1
		adm = 3
		dip = 0
		mil = 1
	}
}